#!/bin/bash

#The purpose of this script is to make a new .java file with the main(String args[]) mess already filled in and open it in vim.

#read -p "Enter name of new .java file, omit the '.java' extension: " name
name=$1
echo "import java.util.Scanner;" >> $name.java
echo "" >> $name.java
echo "/**" >> $name.java
echo "*" >> $name.java
echo "*" >> $name.java
echo "* @author Scott Hasserd" >> $name.java
echo "* @version 1" >> $name.java
echo "*/" >> $name.java
echo "public class" $name "{" >> $name.java
echo " " >> $name.java
echo "    /**" >> $name.java
echo "    * " >> $name.java
echo "    *" >> $name.java
echo "    * @param args command line arguments (not used)" >> $name.java
echo "    */" >> $name.java
echo "    public static void main(String args[]) {" >> $name.java
echo "        " >> $name.java
echo "        " >> $name.java
echo "        " >> $name.java
echo "    }" >> $name.java
echo " " >> $name.java
echo "}" >> $name.java
